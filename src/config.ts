import dotenv from "dotenv";
import path from "path";

dotenv.config({
    path: path.resolve(`config/${process.env.NODE_ENV}.env`)
});

const NODE_ENV = process.env.NODE_ENV || 'development';

const HOST = process.env.HOST || 'localhost';
const PORT = process.env.PORT || 8080;

const DB_HOST = process.env.DB_HOST || 'localhost';
const DB_USERNAME = process.env.DB_USERNAME || 'root';
const DB_PASSWORD = process.env.DB_PASSWORD || 'reroot';
const DB_NAME = process.env.DB_NAME || 'Storage';

const JWT_KEY = process.env.JWT_KEY || 'DhPXymGI-iv1p-FmJ8-Ho0P-3OusTX5e';

const API_URL = `http://${HOST}${PORT ? (':' + PORT) : ''}`;

export {
    NODE_ENV,
    HOST,
    PORT,
    DB_HOST,
    DB_USERNAME,
    DB_PASSWORD,
    DB_NAME,
    API_URL,
    JWT_KEY
}
import "reflect-metadata";
import express, { Response, Request, NextFunction, Application} from "express";
import { HOST, PORT } from "./config";
import router from "./api/routes";

const baseUrl = '/shopping';


const app: Application = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(baseUrl, router);

try
{
    app.listen(Number(PORT), () => {
        console.dir(`http://${HOST}:${PORT}`);
    });
}
catch(error)
{
    console.dir(error);
}

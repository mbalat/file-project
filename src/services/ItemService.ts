import { IItem } from "../interfaces/IItem";
import ItemModel from "../repository/models/ItemModel";
import fs from 'fs';
import database from "../repository";
import csvWrite from "../utils/csvWriter";

export default class ItemService
{
    FindAsync = async (): Promise<IItem[]> =>
    {
        return ItemModel.findAll();
    }

    GetAsync = async (id: string): Promise<IItem | null> =>
    {
        return await ItemModel.findByPk(id);
    }

    PostAsync = async (body: IItem): Promise<IItem> =>
    {
        return await ItemModel.create(body); 
    }

    PutAsync = async (id: string, model: IItem): Promise<boolean> =>
    {
        return (await ItemModel.update(model, {where: {id: id}}))[0] > 0;
    }

    DeleteAsync = async (id: string): Promise<boolean> =>
    {
        return (await ItemModel.destroy({ where: {id: id}})) > 0;
    }

    UploadAsync = async (filename: string): Promise<IItem[] | null> =>
    {
        const csv = await require("fast-csv");
        let tutorials: IItem[] = [];
        let path = "./public/uploads/csv/" + filename;

        let result = await fs
        .createReadStream(path)
        .pipe(await csv.parse({ headers: true}))
        .on("data", async (row: any) => {
            tutorials.push(row);
        })
        .on("end", async () => {
            const transaction = await database.transaction();
            try {
                await ItemModel.bulkCreate(tutorials, { transaction });
                await transaction.commit();
                if(tutorials.length > 0)
                   return tutorials;
            } catch (error) {
                await transaction.rollback();
                console.dir(error);
                return null;
            }
        });

        return result;
    }

    ExportDataAsync = async (filename: string): Promise<boolean> =>
    {
        const databaseResponse: IItem[] = await this.FindAsync();
        const result = await csvWrite(databaseResponse, filename)
        .then(() => {
            return true
        })
        .catch((error) => {
            if(error)
                return false;
        });

        return result!;
    }
}
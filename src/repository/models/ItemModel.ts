import { DataTypes, Model, Optional, UUIDV4 } from "sequelize";
import database from "..";


export interface IItemAttributes
{
    id: string;
    title: string;
    price: number;
    dateCreated: string;
}

export interface IItemCreate extends Optional<IItemAttributes, "id"> {}

interface IItemInstance extends Model<IItemAttributes, IItemCreate>, IItemAttributes {}

const ItemModel = database.define<IItemInstance>("item", {
    id: {
        type: DataTypes.UUID,
        defaultValue: UUIDV4,
        primaryKey: true,
        allowNull: false
    },
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },
    price: {
        type: DataTypes.FLOAT(5,2),
        allowNull: false
    },
    dateCreated: {
        type: DataTypes.DATEONLY,
        allowNull: false
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: "Item"
});

export default ItemModel;
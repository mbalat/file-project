import { DataTypes, Model, Optional, UUIDV4 } from "sequelize";
import database from "..";


export interface ITutorialAttributes
{
    id: string;
    title: string;
    description: string,
    published: boolean,
    createdAt: string,
    updatedAt: string
}

export interface ITutorialCreate extends Optional<ITutorialAttributes, "id"> {}

interface ITutorialInstance extends Model<ITutorialAttributes, ITutorialCreate>, ITutorialAttributes {}

const ITutorialModel = database.define<ITutorialInstance>("tutorial", {
    id: {
        type: DataTypes.UUID,
        defaultValue: UUIDV4,
        primaryKey: true,
        allowNull: false
    },
    title: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    published: {
        type: DataTypes.BOOLEAN,
        allowNull: false
    },
    createdAt: {
        type: DataTypes.DATEONLY,
        allowNull: false,
    },
    updatedAt: {
        type: DataTypes.DATEONLY,
        allowNull: false
    }
}, {
    timestamps: false,
    freezeTableName: true,
    tableName: "Tutorial"
});

export default ITutorialModel;
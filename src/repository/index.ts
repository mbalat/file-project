import { Dialect, Sequelize } from "sequelize";
import { DB_NAME, DB_PASSWORD, DB_USERNAME } from "../config";

const database = new Sequelize({
    dialect: 'mysql' as Dialect,
    database: DB_NAME,
    username: DB_USERNAME,
    password: DB_PASSWORD,
    dialectOptions: {
        dateStrings: true,
        typeCast: ((field: any, next: any) => {
            if(field.type === 'DATETIME')
                return field.string();

            return next();
        })
    },
    timezone: '+02:00'
});

// database.sync();

export default database;
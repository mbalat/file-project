import { Request, Response } from "express";
import { IFileName } from "../../interfaces/IItemFile";
import ItemService from "../../services/ItemService";
import csvRead from "../../utils/csvReader";
import csvWrite from "../../utils/csvWriter";
import path from 'path';
import fs from 'fs';
import fileReader from "../../utils/fileReader";

export default class ItemController
{
    private _service: ItemService = new ItemService();

    FindAsync = async (req: Request, res: Response): Promise<Response> =>
    {
        const result = await this._service.FindAsync();
        return res.status(200).json(result);
    }

    GetAsync = async (req: Request, res: Response): Promise<Response> =>
    {
        const id = req.params.id;
        const result = await this._service.GetAsync(id);
        return res.status(200).json(result);
    }

    PostAsync = async (req: Request, res: Response): Promise<Response> =>
    {
        const body = req.body;
        const result = await this._service.PostAsync(body);
        return res.status(200).json(result);
    }

    PutAsync = async (req: Request, res: Response): Promise<Response> =>
    {
        const id = req.params.id
        const model = await this._service.GetAsync(id)
        if(!model)
            return res.status(404).json("Item not found");
        
        const body = req.body;
        
        Object.assign(model, body);
        const parsedModel = await JSON.parse(JSON.stringify(model));
        const result = await this._service.PutAsync(id, parsedModel);
        if(!result)
            return res.status(500).json("Error on update");

        return res.status(200).json(body);
    }

    DeleteAsync = async (req: Request, res: Response): Promise<Response> =>
    {
        const id = req.params.id;
        const model = await this._service.GetAsync(id);
        if(!model)
            return res.status(404).json("Item not found");
        
        const result = await this._service.DeleteAsync(id);
        if(!result)
            return res.status(500).json("Error on delete");

        return res.status(200).json("Delete success");
    }

    // read data from database and write in file
    WriteInFileAsync = async (req: Request, res: Response): Promise<Response> =>
    {
        const body: IFileName = req.body;
        const directoryPath = path.resolve('.') + `/public/uploads/csv/`;
        let existingFileName = await fileReader(directoryPath, body.filename);
        if(existingFileName)
            return res.status(500).json("File with given name already exists.");
        
        const result = await this._service.FindAsync();
        if(!result)
            return res.status(500).json("Error on read.");

        csvWrite(JSON.parse(JSON.stringify(result)), body.filename);
        
        return res.status(200).json(result);
    }

    //read data from file and return it in response format
    ReadFromFileAsync = async (req: Request, res: Response): Promise<Response> =>
    {
        const body: IFileName = req.body;
        const directoryPath =  path.resolve('.') + `/public/uploads/csv/`;
        let existingFileName =  await fileReader(directoryPath, body.filename);
        if(!existingFileName)
            return res.status(500).json("File with given name does not exists.");

        let result = await csvRead(body.filename);
        if(!result)
            return res.status(500).json("Error on file reading.");

        return res.status(200).json(result);
    }

    // upload file and insert data into database
    UploadAsync = async (req: Request, res: Response): Promise<Response> =>
    {
        if(req.file === undefined)
            return res.status(400).json("Please upload a CSV file!");
            
        let result = await this._service.UploadAsync(req.file.filename);
        if(!result)
            return res.status(500).json({message: "Could not upload the file: " + req.file?.originalname});

        return res.status(200).json({message: "Success."});
    }

    //read data from database and insert into file
    ExportDataAsync = async (req: Request, res: Response): Promise<Response> =>
    { 
        const body: IFileName = req.body;
        const result = await this._service.ExportDataAsync(body.filename);
        if(!result)
            return res.status(500).json("Error on writing data from database to file.");

        return res.status(200).json(`Data saved in to ${body.filename}.csv file.`);
    }

    DownloadAsync = async (req: Request, res: Response) =>
    {
        //res.download();
    }
}
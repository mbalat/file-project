import { Expose } from "class-transformer";
import { IsDate, IsDecimal, IsDefined, IsNotEmpty } from "class-validator";

export class Item
{
    @Expose()
    id: string;

    title: string;

    @Expose()
    @IsDecimal({decimal_digits: '2'})
    @IsDefined()
    @IsNotEmpty()
    price: number;

    @Expose()
    @IsDefined()
    @IsDate()
    dateCreated: Date;
}
import { Expose } from "class-transformer";
import { IsDefined, IsString } from "class-validator";

export class ItemFile
{
    @Expose()
    @IsDefined()
    @IsString()
    filename: string;
}
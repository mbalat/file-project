import { plainToClass } from "class-transformer"
import { validate } from "class-validator";
import { NextFunction, Request, Response } from "express"

const validateBody = (bodyClass: any) =>
{
    return async (req: Request, res: Response, next: NextFunction) =>
    {
        const output: any = plainToClass(bodyClass, req.body);
        await validate(output, {skipMissingProperties: true}).then(errors => {
            if(errors.length > 0)
            {
                let errorTexts = Array();
                for(const errorItem of errors) errorTexts = errorTexts.concat(Object.values(errorItem.constraints!)).map((x: string) => x.charAt(0).toUpperCase() + x.slice(1));
                res.status(400).send(errorTexts);
                return;
            }
            else
            {
                req.body = output;
                next();
            }
        })
    }
}

export default validateBody;
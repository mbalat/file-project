import { Request } from 'express';
import multer from 'multer';
import path from 'path';
import { checkExistance, createPath } from '../../utils/fileHelper';

const constructMulter = (destination: string) =>
{
    const storage = multer.diskStorage({
        destination: (req: Request, file: Express.Multer.File, cb: any) =>
        {
            const basePath = './public/uploads/'
            if(checkExistance(basePath))
                createPath(basePath);

            const dir = basePath + destination;
            if(checkExistance(dir))
                createPath(basePath);

            cb(null, dir);
        },

        filename: (req: Request, file: Express.Multer.File, cb: any) =>
        {
            cb(null, file.originalname);
        },
    });

    const fileFilter = (req: Request, file: Express.Multer.File, cb: any) =>
    {
        if(file.mimetype === 'text/csv')
        {
            cb(null, true)
        }
        else
        {
            cb("Please upload only csv file.", false);
        }
    }

    return multer({
        storage: storage,
        fileFilter: fileFilter
    })
}

export default constructMulter;
import { Router } from "express";
import ItemController from "../controllers/ItemController";
import constructMulter from "../middlewares/upload";
import validateBody from "../middlewares/validateBody";
import { ItemFile } from "../validators/Item/ItemFile";

const upload = constructMulter;
const itemController = new ItemController();
const itemRouter = Router();

itemRouter.get("/", itemController.FindAsync);
itemRouter.get("/export", itemController.ExportDataAsync);
itemRouter.get("/read", validateBody(ItemFile), itemController.ReadFromFileAsync);
itemRouter.get("/write", itemController.WriteInFileAsync);
itemRouter.get("/download", itemController.DownloadAsync);
itemRouter.get("/:id", itemController.GetAsync);
itemRouter.post("/", itemController.PostAsync);
itemRouter.post("/upload", upload('csv').single('file'), itemController.UploadAsync);
itemRouter.put("/:id", itemController.PutAsync);
itemRouter.delete("/:id", itemController.DeleteAsync);

export default itemRouter;
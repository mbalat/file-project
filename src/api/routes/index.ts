import { Router } from "express";
import itemRouter from "./itemRoute";

const router = Router();

router.use("/item", itemRouter);

export default router;
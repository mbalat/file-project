import fs from 'fs';
import { HOST, PORT } from '../config';

const getApiUrl = (): string => {
    return `http://${HOST}:${PORT}/shopping/`;
}

const getPublicFolder = (): string =>
{
    if (/^win/i.test(process.platform)) {
        return "public\\"
    } else {
        return "public/"
    }
}

const getFileUrl = (location: string): string =>
{
    const apiUrl = getApiUrl();
    const publicFolder = getPublicFolder();
    return apiUrl + location.replace(publicFolder, "");
}

const getImageLocation = (url: string): string =>
{
    const apiUrl = getApiUrl();
    const publicFolder = getPublicFolder();
    const res = publicFolder + url.replace(apiUrl, "");
    return res;
}

const checkExistance = (path: string): boolean =>
{
    return fs.existsSync(path);
}

const deleteImage = (path: string) =>
{
    if(checkExistance(path))
        fs.unlinkSync(path);
    else
        console.dir(`File path ${path} not found.`);
}

const createPath = (path: string) =>
{
    if(!checkExistance(path))
        fs.mkdirSync(path);
}

export {
    getApiUrl,
    getPublicFolder,
    getFileUrl,
    getImageLocation,
    checkExistance,
    deleteImage,
    createPath
}
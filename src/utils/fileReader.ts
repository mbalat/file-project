import fs from 'fs';

const fileReader = async (directoryPath: string, filename: string): Promise<boolean> =>
{
    return fs.readdirSync(directoryPath).includes(`${filename}.csv`);
}

export default fileReader;
import csv from 'csvtojson'

let csvRead = async (fileName: string): Promise<any> =>
{
    const path = "./public/uploads/csv/" + fileName + ".csv";
    let result = await csv().fromFile(path)
    .then((jsonObj: any) => {
        if(!jsonObj)
        {
            return null;
        }
        else
        {
            return jsonObj;
        }
    });
    return result;
}

export default csvRead;
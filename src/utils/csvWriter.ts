import * as fs from 'fs';
import { json2csv } from 'json-2-csv';

let csvWrite = async (data: any, fileName: string): Promise<void> =>
{
    const path = "./public/uploads/csv/" + fileName + ".csv";
    await json2csv(JSON.parse(JSON.stringify(data)), (err: any, csv: any) => {
                if(err)
                    throw err;

                console.log(csv);

                fs.writeFileSync(path, csv);
            });
}

export default csvWrite;